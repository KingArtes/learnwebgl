var vertexShaderText =
    [
        'precision mediump float;',
        '',
        'attribute vec3 vertPosition;',
        'attribute vec3 vertColor;',
        'uniform mat4 mWorld;',
        'uniform mat4 mView;',
        'uniform mat4 mProj;',
        'varying vec3 fragColor;',
        '',
        'void main() {',
        '   fragColor = vertColor;',
        '   gl_Position = mProj * mView * mWorld * vec4(vertPosition, 1.0);',
        '}'
    ].join('\n');

var fragmentShaderText =
    [
        'precision mediump float;',
        '',
        'varying vec3 fragColor;',
        'void main() {',
        '   gl_FragColor = vec4(fragColor, 1.0);',
        '}'
    ].join('\n');

InitDemo = () => {
    console.log("This is working");

    var canvas = document.getElementById('game-surface');
    var gl = canvas.getContext('webgl');

    if(!gl) {
        console.log('WebGL not supported without, falling back on experimental webgl');
        gl = canvas.getContext('experemental-webgl');
    }

    if(!gl) {
        alert('Your browser does not support WebGL');
    }

    // canvas.width = window.innerWidth;
    // canvas.height = window.innerHeight;
    // gl.viewport(0, 0, window.innerWidth, window.innerHeight);

    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // create shaders
    // ----------------
    var vertexShader = createShader(gl, vertexShaderText, gl.VERTEX_SHADER);
    var fragmentShader = createShader(gl, fragmentShaderText, gl.FRAGMENT_SHADER);

    // form a program
    var program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if(!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(program));
        return;
    }
    // this is an expensive check. Usually done during debugging
    gl.validateProgram(program);
    if(!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error('ERROR validating program!', gl.getProgramInfoLog(program));
    }

    // Create buffers
    // --------------
    // those are 64-bit precision floats
    var triangleVertices =
        [    // x, y, z      // R G B
             0.0, 0.5, 0.0,  1.0, 1.0, 0.0,
            -0.5, -0.5, 0.0, 0.7, 0.0, 1.0,
             0.5, -0.5,0.0,  0.1, 1.0, 0.6
        ];

    var triangleVertexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexBufferObject);
    // WebGL requires 32-bit floats
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVertices), gl.STATIC_DRAW);

    var positionAttributeLocation = gl.getAttribLocation(program, 'vertPosition');
    var colorAttributeLocation = gl.getAttribLocation(program, 'vertColor');
    gl.vertexAttribPointer(
        positionAttributeLocation, // attrib location
        3,  // number of elements per attribute
        gl.FLOAT, //type of elements
        gl.FALSE, // normalised?
        6 * Float32Array.BYTES_PER_ELEMENT, // size of an individual attribute
        0 // offset from the beginning of a single vertex to this attribute
    );
    gl.enableVertexAttribArray(positionAttributeLocation);

    gl.vertexAttribPointer(
        colorAttributeLocation, // attrib location
        3,  // number of elements per attribute
        gl.FLOAT, //type of elements
        gl.FALSE, // normalised?
        6 * Float32Array.BYTES_PER_ELEMENT, // size of an individual attribute
        3 * Float32Array.BYTES_PER_ELEMENT // offset from the beginning of a single vertex to this attribute
    );
    gl.enableVertexAttribArray(colorAttributeLocation);

    gl.useProgram(program);

    // set uniforms
    var matWorldUniformLocation = gl.getUniformLocation(program, "mWorld");
    var matViewUniformLocation = gl.getUniformLocation(program, "mView");
    var matProjUniformLocation = gl.getUniformLocation(program, "mProj");

    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projMatrix = new Float32Array(16);
    glMatrix.mat4.identity(worldMatrix);
    glMatrix.mat4.lookAt(viewMatrix, [0, 0, -2], [0, 0, 0], [0, 1, 0]);
    // this glMatrix.glMatrix.toRadian(45) is a bruh moment
    glMatrix.mat4.perspective(projMatrix, glMatrix.glMatrix.toRadian(45), canvas.width / canvas.height, 0.1, 1000.0);

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMatrix);

    // Main render loop
    // ----------------
    var identityMatrix = new Float32Array(16);
    glMatrix.mat4.identity(identityMatrix);
    var angle = 0;
    var loop = () => {
        // one full rotation every 6 seconds
        angle = performance.now() / 1000 / 6 * 2 * Math.PI;
        glMatrix.mat4.rotate(worldMatrix, identityMatrix, angle, [0, 1, 0]);
        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);

        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLES, 0, 3);

        requestAnimationFrame(loop);
    };
    requestAnimationFrame(loop);
};

const createShader = (gl, sourceCode, type) => {
    var shader = gl.createShader(type);
    gl.shaderSource(shader, sourceCode);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        var info = gl.getShaderInfoLog(shader);
        throw "ERROR compiling shader of type " + type + "! \n" + info;
    }

    return shader;
};